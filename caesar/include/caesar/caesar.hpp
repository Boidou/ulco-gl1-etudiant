#pragma once
#include <iostream>

std::string lire(std::string path);
char decaler_lettre(char caractere, int clef);
std::string chiffrer(std::string fichier, int clef);
std::string dechiffrer(std::string fichier, int clef);
void ecrire(std::string path, std::string fichiercrypter);