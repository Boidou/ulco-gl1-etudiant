.. Ceasar documentation master file, created by
   sphinx-quickstart on Thu Dec  3 16:16:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ceasar's documentation!
==================================

Description

Le code César est un chiffrement basé sur un décalage de 
l'alphabet (déplacement des lettres plus loin dans l'alphabet), 
il s'agit d'une substitution monoalphabétique, 
c'est-à-dire qu'une même lettre n'est remplacée que par une seule autre (toujours identique pour un même message).

.. image:: ../cesar.png
   :width: 300

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
