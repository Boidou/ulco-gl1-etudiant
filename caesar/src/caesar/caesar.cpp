#include <caesar/caesar.hpp>
#include <iostream>
#include <fstream>

//passage d'un chemin d'un fichier en paramêtre car j'arrive pas avec l'entrée au clavier
std::string lire(std::string path){ 
    std::ifstream fichier(path); 
    std::string str = "";
    std::string line;
    while(getline(fichier,line)){
        str += line + "\n";
    }
    return str;
}

void ecrire(std::string path, std::string fichiercrypter){
    std::ofstream fichier(path.c_str());
    fichier << fichiercrypter;       
}
//Fonction décaler une lettre en fonction d'une clef passé en paramètre.
char decaler_lettre(char caractere, int clef){
    if (int(caractere) > 96 && int(caractere) < 123){  //Table asci : entre 96 et 123 ce sont les lettres minusules
        if (int(caractere)+clef > 122){
            int res = (int(caractere)+clef) - 122;
            int(caractere) = res+96;
            return char(int(caractere));
        }else{return char(int(caractere) + clef);}
    }else{
        return char(int(caractere));
    }
}

std::string chiffrer(std::string fichier, int clef){
   for(int i = 0 ; i < fichier.size(); i++){ // parcourt les caractères du fichier
       fichier[i] = decaler_lettre(fichier[i], clef);
   }
   return fichier;
}

std::string dechiffrer(std::string fichier, int clef){
   for(int i = 0 ; i < fichier.size(); i++){ // parcourt les caractères du fichier
       fichier[i] = decaler_lettre(fichier[i], -clef);
   }
   return fichier;
}