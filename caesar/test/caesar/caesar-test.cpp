#include <caesar/caesar.hpp>
#include <catch2/catch.hpp>

TEST_CASE("test pour decaler une lettre de 5"){
    REQUIRE(decaler_lettre('a',5) == 'f');
}
TEST_CASE ("test pour savoir si il prend en compte ou pas une ponctuation "){
    REQUIRE(decaler_lettre('!',5) == '!');
}
TEST_CASE ("test du chiffrage d'un mot"){
    REQUIRE(chiffrer("test",5) == "yjxy");
}