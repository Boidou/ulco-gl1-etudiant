#include <guess/guess.hpp>
#include <iostream>
using namespace std;


void guessingGame(){
    int guess = rand() % 100 + 1;
    int nombre = 0;
    int essaie = 5;
    int resultat = 0;
  
    while ( essaie != 0 || resultat == 1){
        std::cout << "history:" << nombre << std::endl;
        std::cout << "number? " << std::endl;
        cin >> nombre;
        resultat = condition(guess, nombre);
        if (resultat  == 1){
            break;
        }else if (essaie == 0){
            std::cout << "La réponse: " << guess << std::endl;
            break;
        }
        essaie = essaie - 1;
    }
}

int condition(int guess, int nombre){
    int resultat = 0;
    if ( nombre > guess ){
        std::cout << "Too high !" << std::endl;
        return resultat;
    }else if  ( nombre < guess ){
        std::cout << "Too low !" << std::endl;
        return resultat;
    }else if ( nombre == guess ){
        std::cout << "T'as gagné" << std::endl;
        resultat = 1;
        return resultat;
    }
}