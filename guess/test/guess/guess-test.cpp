#include <guess/guess.hpp>
#include <catch2/catch.hpp>


TEST_CASE( "Test Too high de la fonction condition" ) {
    REQUIRE( condition(90,95) );
}

TEST_CASE( "Test Too low de la fonction condition" ) {
    REQUIRE( condition(60,19) );
}