/// \mainpage Documentation de drunk_player
///
/// \brief Drunk_player est un système de lecture de vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux, aléatoirement et parfois en transformant l'image.
///
/// Drunk_player utilise la bibliothèque de traitement d'image OpenCV et est composé :