#include "MyClass.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init & getter", "[MyClass]" ) {
    MyClass myclass;
    REQUIRE( myclass.mydata() == "");
}

TEST_CASE( "setter", "[MyClass]" ) {
    MyClass myclass;
    myclass.mydata() = "tata";
    REQUIRE( myclass.mydata() == "tata");
}

TEST_CASE( "reset", "[MyClass]" ) {
    MyClass myclass;
    myclass.mydata() = "ok";
    myclass.reset();
    REQUIRE( myclass.mydata() == "");
}

TEST_CASE( "fail", "[MyClass]" ) {
    MyClass myclass;
    try{
        myclass.fail();
        REQUIRE ( false );
    }
    catch( ... )
    {
        REQUIRE ( true );
    }
    
}

TEST_CASE( "sqrt2", "[MyClass]" ) {
    MyClass myclass;
    const double e = 0.001;
    const double res = myclass.sqrt2();
    REQUIRE ( res < 1.414 + e);
    REQUIRE ( res > 1.414 - e);
}

TEST_CASE( "operator<<", "[MyClass]" ) {
    // TODO tester l'operateur << (après un set).
    // Indication : utiliser std::ostringstream.
}

