#include <catch2/catch.hpp>
#include <mymaths/mymaths.hpp>

TEST_CASE( "Test add2" ) {
    REQUIRE( add2(40) == 42 );
    REQUIRE( add2(1) == 3 );
    REQUIRE( add2(17) == 19 );
    // REQUIRE( false );  // 
}

TEST_CASE( "Test addn" ) {
    REQUIRE( addn(40,15) == 55 );
    REQUIRE( addn(1,6) == 7 );
    REQUIRE( addn(17,2) == 19 );
    // REQUIRE( false );  // 
}