#include <catch2/catch.hpp>
#include <mymaths/mymaths.hpp>

TEST_CASE( "Test mul2" ) {
    REQUIRE( mul2(40) == 80 );
    REQUIRE( mul2(1) == 2 );
    REQUIRE( mul2(10) == 20 );
    // REQUIRE( false );  // 
}

TEST_CASE( "Test muln" ) {
    REQUIRE( muln(40,3) == 120 );
    REQUIRE( muln(1,6) == 6 );
    REQUIRE( muln(17,2) == 34 );
    // REQUIRE( false );  // 
}