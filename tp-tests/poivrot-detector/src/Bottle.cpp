#include "Bottle.hpp"

#include <sstream>

double alcoholVolume(const Bottle & b) {
    return b._vol *b._deg;
}

double alcoholVolume(const std::vector<Bottle> & bs) {
    double b1 = bs[0]._vol * bs[0]._deg;
    double b2 = bs[1]._vol * bs[1]._deg;
    return (b1+b2);
}

bool isPoivrot(const std::vector<Bottle> & bs) {
    // TODO
    return false;
}

std::vector<Bottle> readBottles(std::istream & is) {
    // TODO
    return {};
}

