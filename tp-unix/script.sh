#!/bin/sh

if [ $# -ne 1 ] ; then
	echo "usage: $0 <nb hello>"
	exit
fi
NB_HELLO=$1

for i in `seq ${NB_HELLO}`; do
	echo "hello (${i} of ${NB_HELLO})"
done
